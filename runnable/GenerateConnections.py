import requests
import os
from common.util import *
from collections import OrderedDict
from ssh.vultrAPI import Vultr
from random import randint
from ssh.ssh_group import *
import utils

'''
stops tmux on all servers effectively shuttting down mining.
'''

def main():
    CONNECTIONS_PER_NODE = 76

    result = None

    while result is None:
        result = generate_connections(CONNECTIONS_PER_NODE, True)
        print("***** FAIL - RETRYING *****")


def generate_connections(CONNECTIONS_PER_NODE, force_replace = False):

    outfile = '../data/node_connections_' + str(CONNECTIONS_PER_NODE) + '.json'
    if not force_replace and os.path.exists(outfile):
        return load_json(outfile)

    node_info = OrderedDict(utils.get_all_node_info())
    node_info_arr = []
    for id, info in node_info.items():
        if info['role'] == 'pool':
            del node_info[id]
        else:
            node_info_arr.append(info)

    count = len(node_info)

    node_connections = OrderedDict()
    node_ip_index = OrderedDict()
    node_used_indexes = OrderedDict()

    for id, info in node_info.items():
        node_connections[id] = []
        node_ip_index[info['private_ip']] = info
        node_used_indexes[id] = []

    nodes_unfilled = 2
    sizes = []
    passes = 0
    inside_passes = 0

    while nodes_unfilled > 1 or (inside_passes > CONNECTIONS_PER_NODE * 1000):
        nodes_unfilled = 0
        for id, info in node_info.items():
            this_node_connections = node_connections[id]

            if len(this_node_connections) >= CONNECTIONS_PER_NODE:
                continue

            node_ip = info['private_ip']
            used_indexes = node_used_indexes[id]

            private_ip = None
            index = None
            other_node_connections = None

            while private_ip is None or private_ip == node_ip or private_ip in this_node_connections:
                index = -1
                is_oversize = False
                is_already_present = False

                while (index == -1 or index in used_indexes) or is_oversize or is_already_present:
                    is_oversize = False
                    is_already_present = False
                    index = randint(0, count - 1)
                    other_id = node_info_arr[index]['host']
                    other_node_connections = node_connections[other_id]
                    if len(other_node_connections) >= CONNECTIONS_PER_NODE:
                        is_oversize = True
                    if node_ip in other_node_connections:
                        is_already_present = True

                    # sizes = []
                    inside_passes += 1
                    # for id, connections in node_connections.items():
                    #     sizes.append(len(connections))
                    #
                    # print("inside passes {}, sizes: {}".format(passes, sizes))
                    if inside_passes > CONNECTIONS_PER_NODE * 1000:
                        return None

                private_ip = node_info_arr[index]['private_ip']



            this_node_connections.append(private_ip)
            other_node_connections.append(node_ip)
            if len(this_node_connections) < CONNECTIONS_PER_NODE:
                nodes_unfilled += 1

            this_node_connections.sort()

        sizes = []
        passes += 1
        for id, connections in node_connections.items():
            sizes.append(len(connections))

        print("passes {}, sizes: {}".format(passes, sizes))



    save_json(outfile, node_connections, False, False, 4)

    if nodes_unfilled > 1:
        return None
    return node_connections


if __name__ == '__main__':
    main()