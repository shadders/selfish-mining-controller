import requests
import os
from common.util import *
from ssh.vultrAPI import Vultr
from ssh.ssh_group import *
import utils

'''
Runs the installer on each server to generate a bitcoin address and setup the pool connection
'''

def main():
    ssh_group = utils.get_ssh_group()
    do_new_install(ssh_group)


def do_new_install(ssh_group):
    script1 = "cd /root/pool/node-stratum-pool/"
    script2 = "./pull.sh\n"
    script3 = "./reset-chain.sh"
    script4 = "./install.sh new {} {}"

    def private_ip_modifier(node_id, node_info, script):
        return str(script).format(node_info['private_ip'], node_info['role'])

    result1 = ssh_group.run_on_nodes(script1)
    result2 = ssh_group.run_on_nodes(script2, 'HEAD is now at', None)
    result3 = ssh_group.run_on_nodes(script3)
    result4 = ssh_group.run_on_nodes(script4, 'Phase two install complete', None, command_modifier=private_ip_modifier)

    utils.eval_results([result1, result2, result3, result4])

    print 'done'




if __name__ == '__main__':
    main()