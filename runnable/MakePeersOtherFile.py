import requests
import os
from common.util import *
from ssh.vultrAPI import Vultr
from ssh.ssh_group import *
import runnable.utils

def main():
    node_params = runnable.utils.get_all_node_info()

    infos = []

    for id, info in node_params.items():
        if info['role'] == 'node':
            d = OrderedDict()
            d['host'] = info['private_ip']
            d['port'] = 18444
            d['network'] = 'testnet'
            d['chain'] = 'public'
            d['name'] = "other[{}]".format(id)
            infos.append(d)

    save_json('../data/peers-other.json', infos)



if __name__ == '__main__':
    main()