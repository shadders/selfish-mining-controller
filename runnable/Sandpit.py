import requests
import os
from common.util import *
from ssh.vultrAPI import Vultr
from ssh.ssh_group import *
import utils

'''
Updates from git then runs the installer on each server.
'''

def main():
    ssh_group = utils.get_ssh_group()
    do_new_install(ssh_group)


def do_new_install(ssh_group):

    result1 = ssh_group.run_rpc_on_nodes("getpeerinfo > /tmp/rpc.json")
    result1 = ssh_group.run_get_file_on_nodes('/tmp/rpc.json')


    connections = OrderedDict()
    dups_list = []
    uniques_list = []

    for id, result in result1['results'].items():
        json = json_parse(result['output'])
        conns = []
        connections[id] = conns
        dups = 0
        uniques = 0

        for conn in json:
            ip = conn['addr']
            ip = ip.split(":")[0]
            #if not ip in conns:
            #    conns.append(ip)
            if ip in conns:
                dups += 1
            else:
                uniques += 1
            conns.append(ip)

        conns.sort()
        dups_list.append(dups)
        uniques_list.append(uniques)

    print(json_write(connections))
    print("dups: {}".format(dups_list))
    print("uniques: {}".format(uniques_list))



    print 'done'

if __name__ == '__main__':
    main()