import requests
import os
from common.util import *
from ssh.vultrAPI import Vultr
from ssh.ssh_group import *
import utils

'''
Updates from git then runs the installer on each server.
'''

def main():
    ssh_group = utils.get_ssh_group()
    do_new_install(ssh_group)


def do_new_install(ssh_group):
    script1 = "cd /root/pool/node-stratum-pool/"
    script2 = "./pull.sh"
    script4 = "./rebuild-daemon.sh"

    result1 = ssh_group.run_on_nodes(script1)
    result2 = ssh_group.run_on_nodes(script2, 'HEAD is now at', None)
    result4 = ssh_group.run_on_nodes(script4, "Leaving directory '/root/coinds/bitcoin-sm/src'")

    utils.eval_results([result1, result2, result4])

    print 'done'

if __name__ == '__main__':
    main()