import requests
import os
import gzip
from common.util import *
from ssh.vultrAPI import Vultr
from ssh.ssh_group import *
import utils

'''
Updates from git then runs the installer on each server.
'''

def main():
    ssh_group = utils.get_ssh_group()

    get_file_synchronous(ssh_group, '/root/state/pool.log', 'pool_logs', include_nodes=False, include_pool=True, remove_remote_gz=False)
    get_file_synchronous(ssh_group, '/root/state/minerd.log', 'minerd_logs', remove_remote_gz=False)

    get_file(ssh_group, '/root/pool/one_block_leads.json', 'one_block_leads', include_nodes=False, include_pool=True)
    get_file(ssh_group, '/root/pool/lambdas.json', 'lamdas', include_nodes=False, include_pool=True)
    get_file(ssh_group, '/root/state/blocks_found.json', 'blocks_found', include_pool=True)


def get_file(ssh_group, remote_file, local_dir, include_nodes = True, include_master = False, include_pool = False, compress=False):

    if not local_dir.endswith('/'):
        local_dir += '/'
    local_dir = '../data/' + local_dir

    if not os.path.exists(local_dir):
        os.mkdir(local_dir)

    if include_nodes:
        result_nodes = ssh_group.run_get_file_on_nodes(remote_file)

        for id, result in result_nodes['results'].items():
            if 'output' in result:
                content = result['output']
                file = local_dir + id + '.txt'
                string_to_file(file, content, False)
            else:
                print('no file retrieved for: ' + id)

    if include_master:
        result_master = ssh_group.run_get_file_on_master(remote_file)

        for id, result in result_master['results'].items():
            if 'output' in result:
                content = result['output']
                file = local_dir + id + '.txt'
                string_to_file(file, content, False)
            else:
                print('no file retrieved for: ' + id)

    if include_pool:
        result_pool = ssh_group.run_get_file_on_pool(remote_file)

        for id, result in result_pool['results'].items():
            if 'output' in result:
                content = result['output']
                file = local_dir + id + '.txt'
                string_to_file(file, content, False)
            else:
                print('no file retrieved for: ' + id)



    print 'done'

def get_file_synchronous(ssh_group, remote_file, local_dir, include_nodes = True, include_master = False, include_pool = False, remove_remote_gz=True):

    if not local_dir.endswith('/'):
        local_dir += '/'
    local_dir = '../data/' + local_dir

    if not os.path.exists(local_dir):
        os.mkdir(local_dir)

    nodes = OrderedDict()

    if include_nodes:
        nodes.update(ssh_group.nodes)
    if include_master:
        nodes.update(ssh_group.masters)
    if include_pool:
        nodes.update(ssh_group.pools)

    if len(nodes.items()) > 0:
        compress_cmd = "gzip -fk " + remote_file
        gzip_file = remote_file + ".gz"
        result_compress = ssh_group._run_many(nodes, compress_cmd, None, None)

        result_nodes = ssh_group._run_synchronous(nodes, gzip_file, None, None, get_file=True)

        for id, result in result_nodes['results'].items():
            if 'output' in result:
                tmp_file = result['output']
                target_file = local_dir + id + '.txt'

                # ungzip the tmp file
                with gzip.open(tmp_file, 'rb') as gz:
                    content = gz.read()
                    string_to_file(target_file, content, False)
                os.unlink(tmp_file)

            else:
                print('no file retrieved for: ' + id)

        #remove remote gz files
        if remove_remote_gz:
            result_clean = ssh_group._run_many(nodes, "rm " + gzip_file, None, None)

    print 'done'



if __name__ == '__main__':
    main()