import requests
import os
from common.util import *
from ssh.vultrAPI import Vultr
from ssh.ssh_group import *
from common.credentials import get_credentials


def main():
    vultr = Vultr()
    filtered = vultr.get_servers_by_prefix('sm-', get_credentials()['vultr_server_password'])
    print json_write(filtered, 4)

    ssh_params = get_ssh_params_from_general(filtered)

    ssh_group = SshGroup(params=ssh_params);

    results = ssh_group.run_on_all('cat ~/server_config.json')

    print "cat result: \n" + json_write(results, 4)


    if not results['complete_success']:
        raise Exception('Failed to collect server configs: ')
    else:
        count = 0
        for label, result in results['results'].items():
            vparams = filtered[label]
            try:
                result = json_parse(result['output'])
            except Exception:
                raise
            vparams['node_num'] = count
            vparams['master_ip1'] = result['master_ip1']
            vparams['master_ip2'] = result['master_ip2']
            vparams['sm_pool_ip'] = result['sm_pool_ip']
            vparams['bitcoin_address'] = result['bitcoin_address'] if 'bitcoin_address' in result else 'NO_ADDRESS'
            vparams['rpc_user'] = result['rpc_user']
            vparams['rpc_password'] = result['rpc_password']
            count += 1

    outfile = '../data/node_params.json'
    print 'writing to ' + os.path.abspath(outfile)

    save_json(outfile, filtered, False, False, 4)




def get_ssh_params_from_general(vultr_params):
    ssh_params = OrderedDict()
    for label, node in vultr_params.items():
        params = OrderedDict()
        ssh_params[label] = params
        params['user'] = 'root'
        params['host'] = node['public_ip']
        params['password'] = node['ssh_password']
        params['role'] = node['role']
    return ssh_params



if __name__ == '__main__':
    main()