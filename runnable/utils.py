import requests
import os
import datetime
from common.util import *
from ssh.ssh_group import *
from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException

ssh_group = None
node_params = None
credentials = None
param_file = '../data/node_params.json'
credentials_file = '../data/credentials.json'

def get_all_node_info():
    global node_params, param_file
    if node_params is None:
        node_params = load_json(param_file)
    return node_params

def get_node_info(node_id):
    node_info = get_all_node_info()
    return node_info[node_id] if node_id in node_info else None


def get_ssh_group():
    global ssh_group, param_file
    if ssh_group is None:
        print 'reading params from: ' + os.path.abspath(param_file)
        ssh_group = SshGroup(file=param_file);
    return ssh_group


def eval_results(results):
    has_failed = False
    for result in results:
        if not result['complete_success']:
            has_failed = True

    print json_write(results)

    if has_failed:
        print "***********\n**FAILURE**\n***********"
    else:
        print "***********\n**SUCCESS**\n***********"

    return not has_failed

def getpeerinfos(ssh_group):
    result1 = ssh_group.run_rpc_on_nodes("getpeerinfo > /tmp/rpc.json")
    result1 = ssh_group.run_get_file_on_nodes('/tmp/rpc.json')


    connections = OrderedDict()

    for id, result in result1['results'].items():
        json = json_parse(result['output'])
        conns = []
        connections[id] = conns
        for conn in json:
            ip = conn['addr']
            ip = ip.split(":")[0]
            if not ip in conns:
                conns.append(ip)
        conns.sort()

    return connections

def data_dir():
    return '../data/'

def data_dir_processed():
    return data_dir() + 'processed/'

def log_state(action, extra_data = None, is_start=False, is_stop=False):

    if not is_start and not is_stop:
        raise Exception('attempting to log state change with specifying start or stop')

    height = None
    try:
        rpc = get_rpc()
        height = rpc.getinfo()['blocks']
    except:
        pass

    file = data_dir() + 'actions.json'
    state = OrderedDict();
    state['time'] = datetime.datetime.utcnow().__str__()
    state['blockHeight'] = height
    state['action'] = action
    state['type'] = 'start' if is_start else 'stop'
    state['data'] = extra_data

    json = json_write(state, None) + '\n'
    string_to_file(file, json, True)

rpc = None

def get_rpc():
    global rpc
    if rpc is None:
        rpc_user = get_credentials()['rpc_user']
        rpc_password = get_credentials()['rpc_password']
        rpc = AuthServiceProxy("http://%s:%s@127.0.0.1:18332" % (rpc_user, rpc_password))
        if rpc is None:
            raise Exception("Cannot connect to RPC, you probably forgot to start the tunnel to master server you burke.")
    return rpc