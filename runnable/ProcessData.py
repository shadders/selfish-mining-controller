import requests
import os
import re
import time
from decimal import *
from datetime import datetime
from common.util import *
from ssh.vultrAPI import Vultr
from ssh.ssh_group import *
import utils
from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException



best_block_height = None
sm_pool_ip = "10.99.0.16"

def main():

    BLOCK_TIME = 600
    DIST_INTERVAL = 10

    # SM_COUNT = 3
    # START_HEIGHT = 2000
    # END_HEIGHT = 10000

    SM_COUNT = 0
    START_HEIGHT = 20000
    END_HEIGHT = 30000

    # SM_COUNT = 0
    # START_HEIGHT = 30000
    # END_HEIGHT = 35000
    #
    # SM_COUNT = 4
    # START_HEIGHT = 50000
    # END_HEIGHT = 55000

    # SM_COUNT = 5
    # START_HEIGHT = 57800
    # END_HEIGHT = 58800

    UPDATE_HASHES = True

    rpc = utils.get_rpc()
    info = rpc.getinfo()
    global best_block_height
    best_block_height = int(info['blocks'])

    if END_HEIGHT == -1:
        END_HEIGHT = best_block_height

    # best_block_hash = rpc.getbestblockhash()
    # print('best block: ' + best_block_hash)
    # print(rpc.getblock(best_block_hash))

    raw_data_dir = '../data/'
    processed_data_dir = raw_data_dir + 'processed/'

    blocks = update_block_hashes(rpc, processed_data_dir, save_on_all=True, do_net_update=UPDATE_HASHES)

    parse_minerd_logs(blocks, raw_data_dir, processed_data_dir, START_HEIGHT, END_HEIGHT, NUM_SELFISH_MINERS)

    process_one_block_leads(rpc, blocks, raw_data_dir, processed_data_dir, START_HEIGHT, END_HEIGHT)
    process_block_winners(rpc, blocks, raw_data_dir, processed_data_dir, START_HEIGHT, END_HEIGHT)

    get_avg_block_time(blocks, START_HEIGHT, END_HEIGHT, SM_COUNT)

    get_block_distribution(blocks, DIST_INTERVAL, BLOCK_TIME, processed_data_dir, 'accepted_blocks', START_HEIGHT,
                           END_HEIGHT, SM_COUNT)


def get_block_distribution(blocks, interval_secs, block_secs, processed_data_dir, file_prefix, start_height, end_height, sm_count):

    outfile = "{}time_dist_{}_{}_{}_smcount_{}.csv".format(processed_data_dir, file_prefix, start_height, end_height, sm_count)

    interval_count = block_secs / interval_secs * 10 #allow for blocks up to 10 times longer than average

    ranges = []
    labels = []
    for i in range(interval_count):
        interval_start = i * interval_secs
        interval_end = interval_start + interval_secs
        ranges.append(0)
        if interval_secs == 1:
            labels.append(str(interval_start))
        else:
            labels.append("{}-{}".format(interval_start, interval_end))

    range_count = len(ranges)
    i = 0

    for key, block_info in blocks.items():
        height = int(key)

        if height < start_height or height >= end_height:
            continue

        i +=1
        prev_block_key = str(height - 1)
        if not prev_block_key in blocks:
            continue

        prev_block = blocks[prev_block_key]

        time_diff = block_info['time'] - prev_block['time']
        interval_index = time_diff / interval_secs
        if interval_index > range_count - 1:
            interval_index = range_count - 1
        elif interval_index < 0:
            interval_index = 0
        ranges[interval_index] += 1

    out = "range, count\n"
    for i in range(0, range_count):
        out += "{}, {}\n".format(labels[i], ranges[i])

    string_to_file(outfile, out)


def get_avg_block_time(blocks, start_height, end_height, sm_count):

    start_time = float(blocks[str(start_height)]['time'])
    end_time = float(blocks[str(end_height)]['time'])
    interval = end_time - start_time

    num_blocks = end_height - start_height
    avg_block_time = interval / num_blocks

    print("Block times for interval {} - {}.  Selfish miner count: {}".format(start_height, end_height, sm_count))
    print("Interval: {} secs, Blocks: {}, Avg block time: {}".format(interval, num_blocks, avg_block_time))

def update_block_hashes(rpc, processed_data_dir, save_on_all=False, do_net_update=True):
    blocks = OrderedDict()

    #load from file first if exists
    file = processed_data_dir + 'block_hashes.json'
    best_height_local = 0
    last_height = 0
    if os.path.exists(file):
        local = load_json(file)
        for key in local.keys():
            height = int(key)
            if height != last_height + 1:
                raise Exception('Blocks are not contiguous, delete block data and start from scratch')
            last_height = height
            if height > best_height_local:
                best_height_local = height
            blocks[key] = local[key]

    if not do_net_update:
        return blocks

    #info = rpc.getinfo()

    global best_block_height
    best_height_remote = best_block_height
    best_block_height = best_height_remote
    interval = best_height_remote - best_height_local
    print("got {} local blocks, remote height: {} - {} to fetch".format(best_height_local, best_height_remote, interval))

    if best_height_local + 1 >= best_height_remote:
        return blocks

    # fetched = 0
    # batch_size = 10
    #
    # for height in range(best_height_local + 1, best_height_remote, batch_size):
    #     key = str(height)
    #     max = height + batch_size
    #     max = height if max <= best_height_remote else best_height_remote
    #
    #     hash = rpc.getblockhash(height)
    #     info = rpc.getblock(hash)
    #     fetched += 1
    #     info['difficulty'] = float(info['difficulty'])
    #     print("fetched block {}, hash: {},  diff: {}, fetched: {}/{}".format(height, hash, info['difficulty'], fetched, interval))
    #     blocks[key] = info
    #     if save_on_all and height % 100 == 0:
    #         save_json(file, blocks)

    fetched = 0
    batch_size = 100

    for height in range(best_height_local + 1, best_height_remote, batch_size):
        max = height + batch_size
        max = max if max <= best_height_remote else best_height_remote

        heights = range(height, max)
        commands = [ ["getblockhash", h] for h in range(height, max)]
        hashes = rpc.batch_(commands)
        infos = rpc.batch_( [ ["getblock", h] for h in hashes])

        for hash, info, height in zip(hashes, infos, heights):
            key = str(height)
            fetched += 1
            info['difficulty'] = float(info['difficulty'])
            print(
            "fetched block {}, hash: {},  diff: {}, fetched: {}/{}".format(height, hash, info['difficulty'], fetched,
                                                                           interval))
            blocks[key] = info
            if save_on_all and height % 1000 == 0:
                save_json(file, blocks)

        # hash = rpc.getblockhash(height)
        # info = rpc.getblock(hash)

    save_json(file, blocks)
    return blocks

def process_one_block_leads(rpc, blocks, raw_data_dir, processed_data_dir, start_height, end_height):

    seen = load_file_line_json_and_filter(raw_data_dir + 'one_block_leads/sm-pool.txt', start_height, end_height)
    sm_blocks_found = load_file_line_json_and_filter(raw_data_dir + 'blocks_found/sm-pool.txt', start_height,
                                                     end_height, hash_key='blockHash')

    kept = 0
    replaced = 0
    total = 0
    duplicates = 0
    missing = 0
    processed = dict()

    for height, data in seen.items():
        key = str(height)
        hash = data['hash']

        if hash in processed:
            duplicates +=1
            continue
        processed[hash] = height

        found_block = None
        if key not in sm_blocks_found or sm_blocks_found[key]['blockHash'] != hash:
            missing += 1
            continue

        if not key in blocks:
            raise Exception('one block lead height not found')

        info = blocks[key]
        if info['hash'] == hash:
            kept += 1
        else:
            replaced +=1
        total +=1

    perc = 1 if total == 0 else float(kept) / total

    print("One block leads - sm won: {}, hm won: {}, total: {},  percentage sm: {}".format(kept, replaced, total, perc))
    print("Kicked {} duplicates, {} missing from blocks_found".format(duplicates, missing))

def process_block_winners(rpc, blocks, raw_data_dir, processed_data_dir, start_height, end_height):
    sm_blocks_found = load_file_line_json_and_filter(raw_data_dir + 'blocks_found/sm-pool.txt', start_height, end_height, hash_key='blockHash')
    hm_blocks_found = []

    outfile = processed_data_dir + "block_winners_{}_{}.csv".format(start_height, end_height)
    string_to_file(outfile, 'height, sm_blocks, hm_blocks, sm_orphans, difficulty\n', False)

    sm_blocks = 0
    sm_orphans = 0
    hm_blocks = 0
    hm_orphans = 0
    total_orphans = 0
    total_blocks = 0
    last_difficulty = 0

    for height in range(start_height, end_height):
        key = str(height)
        total_blocks += 1
        if key in sm_blocks_found:
            data = sm_blocks_found[key]
            hash = data['blockHash']
            difficulty = data['blockDiff']
            last_difficulty = difficulty
            live_block = blocks[key]
            live_hash = live_block['hash']
            if live_hash == hash:
                sm_blocks += 1
            else:
                sm_orphans +=1
                total_orphans += 1
                hm_blocks += 1
        else:
            last_difficulty = blocks[key]['difficulty']
            hm_blocks += 1
        line = "{},{},{},{},{}\n".format(height, sm_blocks, hm_blocks, sm_orphans, last_difficulty)
        string_to_file(outfile, line, True)

    perc_sm = float(sm_blocks) / total_blocks
    perc_orphans = 0 if sm_blocks == 0 else float(sm_orphans) / sm_blocks
    print("Block winners - total: {}, sm blocks: {}. hm blocks: {}, sm orphans: {}, perc orphans: {}, perc sm: {}".format(total_blocks, sm_blocks, hm_blocks, sm_orphans, perc_orphans, perc_sm))

    for key, data in sm_blocks_found.items():
        height = int(key)


def parse_minerd_logs(blocks, raw_data_dir, processed_data_dir, start_height, end_height, num_selfish_miners):
    #parse and consolidate hashes by minute
    raw_files = os.listdir(raw_data_dir + 'minerd_logs')

    totals = []
    total = long(0)

    for raw_file in raw_files:
        raw_file = raw_data_dir + 'minerd_logs/' + raw_file
        node_id = os.path.basename(raw_file)
        if not os.path.exists(processed_data_dir + 'minerd_hashes'):
            os.mkdir(processed_data_dir + 'minerd_hashes')
        target_file = processed_data_dir + 'minerd_hashes/' + node_id
        total_for_range = parse_minerd_log(blocks, node_id, raw_file, target_file, start_height, end_height)
        totals.append(total_for_range)
        total += total_for_range

    sm_total = sum(totals[0:num_selfish_miners])
    hm_total = sum(totals[num_selfish_miners:])
    perc = 1 if total == 0 else Decimal(sm_total) / Decimal(total)
    print("totals: {}".format(totals))
    print("sm_totals: {}".format(totals[0:num_selfish_miners]))
    print("hm_totals: {}".format(totals[num_selfish_miners:]))

    print("sm_total: {}, hm_total: {}, total: {}, perc: {}".format(sm_total, hm_total, total, perc))

def parse_minerd_log(blocks, node_id, raw_file, target_file, start_height, end_height):

    global sm_pool_ip

    lines_all = 0
    lines_hash = 0
    current_block_height = 1
    current_block_info = blocks[str(current_block_height)]
    current_block_time = current_block_info['time']
    next_block_time = blocks[str(current_block_height + 1)]['time']
    current_block_hashes = long(0)
    mining_mode = None

    begin_aggregate_block = start_height
    end_aggregate_block = end_height
    aggregate_total = long(0)

    print("Processing {} --> {}".format(raw_file, target_file))

    sm_marker = "Starting Stratum on stratum+tcp://{}".format(sm_pool_ip)
    hm_marker = "Starting Stratum on stratum+tcp://localhost"

    reg_time = re.compile(r"\[([^\]]*)\]")
    reg_hashes = re.compile(r"(\d*) hashes")

    with open(raw_file, 'rU') as fin, open(target_file, 'w') as fout:
        fout.write("timestamp, block, mode, hashes\n")

        for line in fin:
            if sm_marker in line:
                mining_mode = 'sm'
            elif hm_marker in line:
                mining_mode = 'hm'
            else:
                time_matches = reg_time.match(line)
                time_str = time_matches.group(1) if time_matches is not None else None
                line_time = datetime.strptime(time_str, "%Y-%m-%d %H:%M:%S")
                timestamp = time.mktime(line_time.timetuple())
                #server is set to UTC timezone so no need to adjust

                #advance through blocks until we find one where we are in the right time range
                while next_block_time <= timestamp:

                    #write data from previous block
                    fout.write("{},{},{},{}\n".format(current_block_time, current_block_height, mining_mode, current_block_hashes))

                    # reset all current block info to next block.
                    current_block_height += 1
                    current_block_info = blocks[str(current_block_height)]
                    current_block_time = current_block_info['time']
                    next_block_time = blocks[str(current_block_height + 1)]['time']
                    current_block_hashes = 0

                hashes_num = None
                hashes_match = reg_hashes.search(line)
                if hashes_match is not None:
                    lines_hash += 1
                    hashes_str = hashes_match.group(1)
                    hashes_num = long(hashes_str)
                    current_block_hashes += hashes_num
                    if current_block_height >= begin_aggregate_block and current_block_height <= end_aggregate_block:
                        aggregate_total += hashes_num
                    pass


            lines_all += 1
            if lines_all % 100000 == 0:
                print("processed {} lines, found {} hash lines".format(lines_all, lines_hash))

    return aggregate_total



def load_file_line_json_and_filter(file, start_height, end_height, height_key = 'height', hash_key = 'hash'):
    raw = file_to_string(file)
    lines = raw.splitlines()

    seen = OrderedDict()
    # filter duplicates
    for line in lines:
        json = json_parse(line)
        height = int(json[height_key])
        key = str(height)
        if height < start_height or (height > end_height and end_height != -1):
            continue
        hash = json[hash_key]
        if key in seen:
            seen_hash = seen[key][hash_key]
            if seen_hash != hash and False:
                raise Exception("duplicate height with different hash: " + str(height))
        else:
            seen[key] = json
    return seen





if __name__ == '__main__':
    main()