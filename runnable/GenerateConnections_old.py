import requests
import os
from common.util import *
from collections import OrderedDict
from ssh.vultrAPI import Vultr
from random import randint
from ssh.ssh_group import *
import utils

'''
stops tmux on all servers effectively shuttting down mining.
'''

def main():
    CONNECTIONS_PER_NODE = 5

    generate_connections(CONNECTIONS_PER_NODE)


def generate_connections(CONNECTIONS_PER_NODE, force_replace = False):

    outfile = '../data/node_connections_' + str(CONNECTIONS_PER_NODE) + '.json'
    if not force_replace and os.path.exists(outfile):
        return load_json(outfile)

    node_info = OrderedDict(utils.get_all_node_info())
    node_info_arr = []
    for id, info in node_info.items():
        if info['role'] == 'pool':
            del node_info[id]
        else:
            node_info_arr.append(info)

    count = len(node_info)

    node_connections = OrderedDict()
    for id, info in node_info.items():
        node_connections[id] = []

    for id, info in node_info.items():
        used_indexes = []
        this_node_connections = node_connections[id]
        node_ip = info['private_ip']

        # while len(this_node_connections) < CONNECTIONS_PER_NODE:
        #     private_ip = None
        #     index = None
        #
        #     duplicate = False
        #
        #     while private_ip is None or private_ip == node_ip:
        #         index = randint(0, count - 1)
        #         while index in used_indexes:
        #             index = randint(0, count - 1)
        #         other_node_id = node_info_arr[index]['host']
        #         if other_node_id in node_connections:
        #             #check if this node IP is already in their list
        #             if node_ip in node_connections[other_node_id]:
        #                 duplicate = True
        #         private_ip = node_info_arr[index]['private_ip']
        #
        #     used_indexes.append(index)
        #     if not duplicate:
        #         this_node_connections.append(node_info_arr[index]['private_ip'])

        for i in range(CONNECTIONS_PER_NODE):
            private_ip = None
            index = None

            while private_ip is None or private_ip == node_ip:
                index = randint(0, count - 1)
                while index in used_indexes:
                    index = randint(0, count - 1)
                private_ip = node_info_arr[index]['private_ip']

            used_indexes.append(index)
            this_node_connections.append(node_info_arr[index]['private_ip'])

        this_node_connections.sort()

    save_json(outfile, node_connections, False, False, 4)

    return node_connections


if __name__ == '__main__':
    main()