import requests
import os
from common.util import *
from ssh.vultrAPI import Vultr
from ssh.ssh_group import *
import utils

'''
stops tmux on all servers effectively shuttting down mining.
'''

def main():
    ssh_group = utils.get_ssh_group()
    stop_tmux(ssh_group)


def stop_tmux(ssh_group, log=True):

    if log:
        utils.log_state('stop_tmux', is_start=True)

    script1 = "cd /root/pool/node-stratum-pool/"
    script2 = "./stop-tmux.sh"

    result1 = ssh_group.run_on_nodes(script1)
    result2 = ssh_group.run_on_nodes(script2)

    result = utils.eval_results([result1, result2])

    if log:
        utils.log_state('stop_tmux', {'result': result}, is_stop=True)

    print 'done'

if __name__ == '__main__':
    main()