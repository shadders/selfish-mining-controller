import requests
import os
from common.util import *
from ssh.vultrAPI import Vultr
from ssh.ssh_group import *
from GenerateConnections import generate_connections
from StopTmux import stop_tmux
import utils

'''
starts tmux (the daemon, pool and miner) then adds connections
'''

def main():

    CONNECTION_COUNT = 76
    SELFISH_MINER_COUNT = 0

    ssh_group = utils.get_ssh_group()
    #stop_tmux(ssh_group, False)
    start_tmux(ssh_group, CONNECTION_COUNT, SELFISH_MINER_COUNT)


def start_tmux(ssh_group, CONNECTION_COUNT, SELFISH_MINER_COUNT):

    node_connections = generate_connections(CONNECTION_COUNT)

    utils.log_state('start_tmux', {'conn_count': CONNECTION_COUNT, 'sm_count': SELFISH_MINER_COUNT}, is_start=True)

    script1 = "cd /root/pool/node-stratum-pool/"
    script2 = "/root/pool/node-stratum-pool/start-tmux.sh {}"
    script3 = "addnode {} remove".format(utils.get_node_info('sm-master')['private_ip'])
    script3a = "disconnectnode {}".format(utils.get_node_info('sm-master')['private_ip'])
    script4 = "addnode {} remove".format(utils.get_node_info('sm-pool')['private_ip'])
    script4a = "disconnectnode {}".format(utils.get_node_info('sm-pool')['private_ip'])

    script_connect = []
    for i in range(CONNECTION_COUNT):
        script_connect.append("|" + str(i) + "|addnode {} onetry")

    def mine_type_modifier(node_id, node_info, script):
        if node_info['role'] != 'node':
            raise Exception('only node roles are allowed')
        node_num = node_info['node_num']
        node_num = int(node_num)
        type = 'sm-pool' if node_num < SELFISH_MINER_COUNT else 'local-pool'
        return str(script).format(type)

    def connection_modifier(node_id, node_info, script):
        parts = script.split("|")
        index = int(parts[1])
        script = parts[0] + parts[2]
        ip = node_connections[node_id][index]
        return str(script).format(ip)


    results = []

    results.append(ssh_group.run_on_nodes(script1))
    results.append(ssh_group.run_on_nodes(script2, 'DAEMON ONLINE...', use_tmux=True, command_modifier=mine_type_modifier))
    results.append(ssh_group.run_rpc_on_nodes(script3))
    results.append(ssh_group.run_rpc_on_nodes(script3a))
    results.append(ssh_group.run_rpc_on_nodes(script4))
    results.append(ssh_group.run_rpc_on_nodes(script4a))

    for script in script_connect:
        results.append(ssh_group.run_rpc_on_nodes(script, command_modifier=connection_modifier))

    result = utils.eval_results(results)

    utils.log_state('start_tmux', {'result': result}, is_stop=True)

    connections = utils.getpeerinfos(ssh_group)
    conn_list = []
    conn_total = float(0)
    for id, conns in connections.items():
        conn_list.append("{}: {}".format(id, len(conns)))
        conn_total += len(conns)
    conn_avg = conn_total / len(conn_list)
    print(json_write(connections))
    print("connection sumary: {}".format(conn_list))
    print("connection average: {}".format(conn_avg))

    print 'done'

if __name__ == '__main__':
    main()