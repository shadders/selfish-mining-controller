from util import load_json


credentials = None
credentials_file = '../data/credentials.json'

def get_credentials():
    global credentials
    if not credentials:
        global credentials_file
        credentials = load_json(credentials_file)
    return credentials