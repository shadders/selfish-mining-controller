'''
Created on 19Jan.,2017

@author: Steve
'''

import os
import re
import time
import hashlib
from __builtin__ import int
import json
from collections import OrderedDict
import jsonpickle

jsonpickle.set_encoder_options('json', indent=4)
jsonpickle.set_preferred_backend('json')


def time_in_millis():
    return int(round(time.time() * 1000))

def split_strip(str, delimiter=',', strip_chars=None):
    result = [item.strip(strip_chars) for item in str.split(delimiter)]
    return result


def safe_int(s):
    """returns an int if possible to parse it from the given value otherwise None."""
    try:
        return int(s)
    except ValueError:
        return None


def safe_long(s):
    """returns an long if possible to parse it from the given value otherwise None."""
    try:
        return long(s)
    except ValueError:
        return None


def lower_keys(map):
    """return a map with keys converted to lowercase """
    map2 = dict()
    for key, value in map.iteritems():
        map2[key.lower()] = value
    return map2


def ensure_keys(map, key_string, default_value=None):
    """ensure the given dict has all keys, if a key doesn't exist it is given default value"""
    for key in split_strip(key_string):
        if not key in map:
            map[key] = default_value


def ireplace(find, replace, string):
    return re.sub("(?i)" + re.escape(find), replace, string)


def md5_hex(data):
    m = hashlib.new('md5_hex')
    m.update(str(data))
    return m.hexdigest()


def insert_before_extension(path, insertion):
    """Inserts [insertion] before the file extension of the given path."""
    split = os.path.splitext(path)
    return split[0] + insertion + split[1]


def set_extenstion(path, extension):
    """Changes the file extension if needed"""
    split = os.path.splitext(path)
    if not extension.startswith('.'):
        extension = '.' + extension
    return split[0] + extension


def file_to_string(path):
    data = None
    with open(path, 'r') as file:
        data = file.read()
    return data


def string_to_file(path, data, append=True):
    if append:
        with open(path, 'ab') as file:
            file.write(data)
    else:
        with open(path, 'wb') as file:
            file.write(data)


def save_json(file, data, unpicklable=False, append=False, indent=4):
    json_string = json.dumps(data, indent=4)
    # json_string = jsonpickle.encode(data, unpicklable=unpicklable)
    string_to_file(file, json_string, append)


def load_json(file):
    str = file_to_string(file)
    return json_parse(str)

def json_parse(string):
    json_obj = json.loads(string, object_pairs_hook=OrderedDict)
    return json_obj

def json_write(obj, indent=4):
    return json.dumps(obj, indent=indent)

def prompt_for_file(dir):
    files = os.listdir(dir)
    prompt = ''
    i = 0
    for file in files:
        prompt += '[{}] {}\n'.format(i, file)
        i += 1
    prompt += 'Select file: \n'
    index = None
    while index is None:
        index = safe_int(raw_input(prompt))
        if isinstance(index, (int, long)) and index >= len(files):
            index = None
        prompt = 'Invalid selection, choose again: '
    return files[index]


email_regex = re.compile(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")


def is_valid_email(email):
    global email_regex
    return email_regex.match(email) is not None


md5_regex = re.compile(r"(^[a-fA-F\d]{32}$)")


def is_md5_string(string):
    global md5_regex
    return md5_regex.match(string) is not None


class Obj(object):
    pass