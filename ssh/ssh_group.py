import ssh_helper
import ssh_node
import csv
import collections
import threading
from common.util import *
from runnable.GatherServerInfo import get_ssh_params_from_general
from common.credentials import get_credentials
from runnable.utils import get_node_info


def main():
    ssh = ssh_helper.Ssh_Helper('conn1', 'root', get_credentials()['vultr_server_password'], password = get_credentials()['vultr_server_password'])
    ssh_group = SshGroup('params.csv')
    print 'working'
    #results = ssh_group.run_on_nodes('ifconfig | grep 10.99')
    do_command(ssh_group, 'cd /root/pool/node-stratum-pool')
    #do_command(ssh_group, './stop-tmux.sh')
    #do_command(ssh_group, './start-tmux.sh local-pool')
    do_command(ssh_group, 'getinfo', 'blocks', None, True)


def do_command(ssh_group, command, success_marker = None, fail_marker = None, is_rpc = False, ):
    results = None
    if is_rpc:
        results = ssh_group.run_rpc_on_nodes(command)
    else:
        results = ssh_group.run_on_nodes(command)
    errors = ssh_group.has_errors(results)
    print "errors: " + str(errors)
    ssh_group.print_results(results)


class SshGroup(object):

    BITCOIN_CLI = "/root/coinds/bitcoin-sm/src/bitcoin-cli "

    def __init__(self, file = None, params = None):

        if file is not None:
            if file.endswith('.csv'):
                self._params = self.load_csv_file(file)
            elif file.endswith('.json'):
                self._params = self.load_json_file(file)
        elif params is not None:
            self._params = params
        else:
            raise Exception('must specify file or params object')

        self.nodes = collections.OrderedDict()
        self.masters = collections.OrderedDict()
        self.pools = collections.OrderedDict()
        self.all_nodes = collections.OrderedDict()

        for k, v in self._params.items():
            node = ssh_node.Ssh_node(k, v.get('user'), v.get('host'), v.get('password'))
            self.all_nodes[k] = node
            role = v.get('role')
            if role == 'node':
                self.nodes[k] = node
            elif role == 'master':
                self.masters[k] = node
            elif role == 'pool':
                self.pools[k] = node
            else:
                raise Exception('Unknown node type: ' + role)

    def run_rpc_on_nodes(self, command, success_marker = None, fail_marker = None, command_modifier = None):
        command = SshGroup.BITCOIN_CLI + command.strip()
        return self._run_many(self.nodes, command, success_marker, fail_marker, False, command_modifier)

    def run_rpc_on_master(self, command, success_marker = None, fail_marker = None, command_modifier = None):
        command = SshGroup.BITCOIN_CLI + command.strip()
        return self._run_many(self.masters, command, success_marker, fail_marker, False, command_modifier)

    def run_rpc_on_all(self, command, success_marker = None, fail_marker = None, command_modifier = None):
        command = SshGroup.BITCOIN_CLI + command.strip()
        return self._run_many(self.pools, command, success_marker, fail_marker, False, command_modifier)

    def run_rpc_on_all(self, command, success_marker = None, fail_marker = None, command_modifier = None):
        command = SshGroup.BITCOIN_CLI + command.strip()
        return self._run_many(self.all_nodes, command, success_marker, fail_marker, False, command_modifier)

    def run_on_nodes(self, command, success_marker=None, fail_marker=None, use_tmux=False, command_modifier = None):
        return self._run_many(self.nodes, command, success_marker, fail_marker, use_tmux, command_modifier)

    def run_on_master(self, command, success_marker=None, fail_marker=None, use_tmux=False, command_modifier = None):
        return self._run_many(self.masters, command, success_marker, fail_marker, use_tmux, command_modifier)

    def run_on_pool(self, command, success_marker=None, fail_marker=None, use_tmux=False, command_modifier = None):
        return self._run_many(self.pools, command, success_marker, fail_marker, use_tmux, command_modifier)

    def run_on_all(self, command, success_marker = None, fail_marker = None, use_tmux = False, command_modifier = None):
        return self._run_many(self.all_nodes, command, success_marker, fail_marker, use_tmux, command_modifier)

    def run_get_file_on_nodes(self, file):
        return self._run_many(self.nodes, file, None, None, get_file=True)

    def run_get_file_on_master(self, file):
        return self._run_many(self.masters, file, None, None, get_file=True)

    def run_get_file_on_pool(self, file):
        return self._run_many(self.pools, file, None, None, get_file=True)

    def _run_synchronous(self, nodes, command, success_marker, fail_marker, use_tmux = False, command_modifier = None, get_file = False):
        if command_modifier is None:
            def func(node_id, node, command):
                return command
            command_modifier = func

        results = collections.OrderedDict()

        # start command iteration
        for id, node in nodes.items():
            modified_command = command_modifier(id, get_node_info(id), command)
            print("Running{} command [{}]: {}".format(' tmux' if use_tmux else '', id, modified_command))

            ssh = node.conn_tmux if use_tmux else node.conn_main
            result = None
            try:
                if get_file:
                    result = ssh.get_file(modified_command, return_content=False)
                else:
                    result = ssh.run_cmd(modified_command, echo=False)
            except Exception as e:
                result = self._make_fail_result(e, modified_command)

            results[id] = result

        summary = self._summarize_results(results, success_marker, fail_marker)

        return summary

    def _make_fail_result(self, e, command):
        result = Obj()
        result.command = command
        result.lines = []
        result.output = "error: " + e.message
        result.success = False
        result.exception = e
        result.no_json = True
        return result

    def _run_many(self, nodes, command, success_marker, fail_marker, use_tmux = False, command_modifier = None, get_file = False):

        if command_modifier is None:
            def func(node_id, node, command):
                return command
            command_modifier = func

        threads = collections.OrderedDict()

        #start threads
        for id, node in nodes.items():
            modified_command = command_modifier(id, get_node_info(id), command)
            print("Running{} command [{}]: {}".format(' tmux' if use_tmux else '', id, modified_command))
            ssh_thread = SshThread(id, node, modified_command, use_tmux, get_file)
            threads[id] = ssh_thread
            ssh_thread.start()

        #join and wait for stop
        for id, ssh_thread in threads.items():
            ssh_thread.join()

        #collect results
        results = collections.OrderedDict()
        for id, ssh_thread in threads.items():
            results[id] = ssh_thread.result

        summary = self._summarize_results(results, success_marker, fail_marker)

        return summary

    def _summarize_results(self, results, success_marker, fail_marker):
        summary = OrderedDict()

        hard_fails = 0
        hard_successes = 0
        unknowns = 0
        node_count = 0

        for key, result in results.items():
            node_count += 1
            try:
                output = result['output'] if 'output' in result else None
                success = result['success'] if 'success' in result else False
                if success_marker is not None and success_marker in output:
                    hard_successes += 1
                elif fail_marker is not None and fail_marker in output:
                    hard_fails += 1
                elif success_marker is None and success:
                    hard_successes += 1
                elif not success:
                    hard_fails += 1
                else:
                    unknowns += 1
            except Exception as err:
                raise

        summary['node_count'] = node_count
        summary['hard_fails'] = hard_fails
        summary['hard_successes'] = hard_successes
        summary['unknowns'] = unknowns
        summary['complete_success'] = hard_successes == summary['node_count']

        summary['results'] = results
        return summary

    def has_errors(self, results):
        """
        Takes a result set and checks if it has any error
        :param results:
        :return: True is any node had an error
        """
        for id, result in results.items():
            if not result.success:
                return True
        return False

    def print_results(self, results):
        for id, result in results.items():
            print "[{}]: {}".format(id, result.output)

    def load_json_file(self, file):
        json = load_json(file)
        params = get_ssh_params_from_general(json)
        return params

    def load_csv_file(self, file):

        params = collections.OrderedDict()

        with  open(file, 'rU') as fin:
            # write headers
            headers_in = 'id, role, user, host, password'
            headers_in = [col.strip() for col in headers_in.split(',')]

            csvin = csv.DictReader(fin, fieldnames=headers_in)
            csvin._fieldnames = headers_in
            csvin.next() #skip headers


            for row in csv.DictReader(fin, fieldnames=headers_in):
                params[row.get('id')] = row

                # cleanup cr in last row
                for k, v in row.items():
                    row[k] = '' if v is None else v.strip()

        return params


class SshThread(threading.Thread):

    def __init__(self, node_id, node, command, is_tmux = False, is_get_file = False, is_get_file_content=True):
        super(SshThread, self).__init__()
        self.id = node_id
        self.node = node
        self.command = command
        self.is_tmux = is_tmux
        self.is_get_file = is_get_file
        self.is_get_file_content = is_get_file_content

    def run(self):
        ssh = self.node.conn_tmux if self.is_tmux else self.node.conn_main
        try:
            if self.is_get_file:
                self.result = ssh.get_file(self.command, return_content=self.is_get_file_content)
            else:
                self.result = ssh.run_cmd(self.command, echo=False)
        except Exception as e:
            self.result = self._make_fail_result(e)

    def _make_fail_result(self, e):
        result = Obj()
        result.command = self.command
        result.lines = []
        result.output = "error: " + e.message
        result.success = False
        result.exception = e
        result.no_json = True
        return result

if __name__ == '__main__':
    main()