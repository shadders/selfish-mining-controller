import requests
from common.util import *
from common.credentials import get_credentials

def main():
    vultr = Vultr()
    filtered = vultr.get_servers_by_prefix('sm-', get_credentials()['vultr_server_password'])
    print json_write(filtered, 4)


class Vultr(object):

    API_TOKEN = get_credentials()['vultr_api_token']

    def __init__(self):
        pass

    def list_servers(self):
        url = "https://api.vultr.com/v1/server/list"
        response = requests.get(url, None, headers={'API-Key': Vultr.API_TOKEN})
        return response

    def get_servers_by_prefix(self, prefix, ssh_password):
        response = self.list_servers()
        json = response.json()
        filtered = OrderedDict()
        for key in json.keys():
            print key
            srv = json[key]
            label = str(srv['label'])
            if label.startswith(prefix):
                record = OrderedDict()
                record['host'] = label
                record['private_ip'] = srv['internal_ip']
                record['public_ip'] = srv['main_ip']
                record['ram'] = srv['ram']
                record['status'] = srv['power_status']
                if label.endswith('pool'):
                    record['role'] = 'pool'
                elif label.endswith('master'):
                    record['role'] = 'master'
                else:
                    record['role'] = 'node'
                record['ssh_password'] = ssh_password

                filtered[label] = record
        return OrderedDict(sorted(filtered.items()))


if __name__ == '__main__':
    main()