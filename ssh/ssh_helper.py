import sshed
from sshed import servers
from sshed.servers import Server
from sshed.commands import Command
from common.util import *
from timeit import timeit
from runnable.utils import *

import paramiko as ssh
from sshed.commands import Command
from getpass import getpass, getuser
from paramiko.proxy import ProxyCommand

def main():
    ssh = Ssh_Helper('conn1', 'root', '45.76.15.195', password = get_credentials()['vultr_server_password'])
    result = ssh.run_cmd('ls');
    print result.output

    result = ssh.run_cmd('ifconfig');
    print result.output

    result = ssh.run_cmd('~/coinds/bitcoin-sm/src/bitcoin-cli getinfo')
    print result.output
    print str(result.to_json())

### add a to_json method to the return object of ssh.run()
def to_json(self):

    if hasattr(self, 'not_json') and self.not_json:
        return None

    try:
        self.json = json_parse(self.output)
    except Exception as e:
        self.not_json = True;
        return None
    return self.json

sshed.commands.Command.to_json = to_json

class Ssh_Helper(object):

    def __init__(self, id, user, host, password = None, priv_key = None):
        self.id = id
        self.user = user
        self.host = host
        self.password = password
        self.priv_key = priv_key
        self.conn = None

    def _get_ssh(self):

        if self.conn is not None:
                return self.conn

        self.conn = sshed.servers.Server(user=self.user, hostname=self.host, password=self.password, timeout=5)
        return self.conn

    def get_file(self, file, return_content=True):
        temp_file = "/tmp/remote_file_{}_{}".format(self.host, self.id)

        ssh = self._get_ssh();
        if ssh is None:
            raise Exception('Could not get valid ssh connection');

        result = OrderedDict()

        try:
            ssh.download(file, temp_file)
            if return_content:
                result['output'] = file_to_string(temp_file)
                os.unlink(temp_file)
            else:
                result['output'] = temp_file
            result['success'] = True
        except Exception as err:
            result['ouput'] = None
            result['success'] = False

        return result


    def run_cmd(self, cmd, echo=False, retries=3):
        """
        Attempts to run an ssh command.  If the connection is not up yet or has failed attempts to open a new connection.
        :param cmd:
        :param echo:
        :return: a result object with properties 'lines' - a list of output lines, 'output' - a single string
        """
        result = None
        tries = 0
        while tries <= retries:

            ssh = self._get_ssh();
            if ssh is None:
                raise Exception('Could not get valid ssh connection');
            try:
                result = ssh.run(cmd, echo=echo)
                result = self._clean_result(result)
                result['cmd'] = cmd
                #if result['output'] == '':
                #    raise Exception('no result, retrying')
                return result
            except Exception as e:
                #clear the connection so next call opens a new one
                self.conn = None
                if tries == retries:
                    raise e

            tries += 1
        return None

    def check_connection(self):
        if self.conn is None:
            return False

        start = time_in_millis()
        result = self.conn.run('whoami', echo = False)
        time = time_in_millis() - start
        self._clean_result(result)
        connection_valid = result.output == self.user;
        print('ssh result: ' + result.output + ' valid: ' + str(connection_valid) + ' time: ' + str(time) + 'ms');

        return connection_valid

    def _clean_result(self, result):
        """
        Tidies up a result object from ssh.run(cmd)
        :param result:
        :return: modified result object
        """
        result.lines = result.output
        result.output = "\n".join(result.lines)
        result.success = result.returncode == 0

        newr = OrderedDict()
        newr['host'] = result.host.username + '@' + result.host.hostname
        newr['command_str'] = result.command_str
        newr['lines'] = result.lines
        newr['output'] = result.output
        newr['returncode'] = result.returncode
        newr['success'] = result.success

        return newr

# replace the default run method
def run(self, command, pty=False, echo=False, timeout=10):
    """
    run should not treat sudo commands any different then normal
    user commands.

        Need to exapnd this for failed sudo passwords and refreshing
        the channel.
        """
    cmd_obj = Command(command, self)

    cd = False

    if 'sudo' in command:
        if self.password is None:
            self.password = getpass('sudo: ')

    if command.startswith('cd'):
        cd, seperator, path = command.partition(' ')

        if not path.startswith(('~', '$', '/')):
            self.cwd = self.cwd + '/' + path
        else:
            self.cwd = path

    channel = self.client.get_transport().open_session(timeout=timeout)

    if self.config.get('forwardagent', False):
        agent = ssh.agent.AgentRequestHandler(channel)

    if pty:
        channel.get_pty()

    if cd:
        channel.exec_command('cd %s' % (self.cwd))
    else:
        channel.exec_command('cd %s &&' % (self.cwd) + command)

    output = []
    while not channel.exit_status_ready():
        if channel.recv_ready():
            received = channel.recv(2048).splitlines()
            output.extend(received)

            if echo:
                for line in received:
                    print line

            has_sudo = [line for line in received if 'sudo' in line]
            if has_sudo:
                channel.sendall(self.password + '\n')

            has_passphrase = [
                line for line in received if 'passphrase' in line]

            if has_passphrase:
                if self.password is None:
                    channel.sendall(getpass(has_passphrase[0])
                                    + '\n')
                else:
                    channel.sendall(self.password + '\n')

    cmd_obj.output = [line for line in output if line]
    cmd_obj.returncode = channel.recv_exit_status()
    channel.close()

    if self.config.get('forwardagent', False):
        agent.close()

    return cmd_obj

#replace default run method
sshed.servers.Server.run = run

if __name__ == '__main__':
    main()