import collections
import threading
from common.util import *
from ssh_group import SshGroup


def main():
    actions = SmActions(SshGroup('params.csv'))


class SmActions(object):

    ssh_group = None

    def __init__(self, ssh_group = None):
        self.ssh_group = SshGroup('params.csv')
        # if ssh_group is None:
        #
        # else:
        #     self.ssh_group = ssh_group


    def start_tmux(self, num_selfish):
        result1 = self.ssh_group.run_on_nodes(Commands.pool_cmd('stop-tmux.sh'))
        result2 = self.ssh_group.run_rpc_on_master(Commands.pool_cmd('stop-tmux.sh'))
        return self._get_result(result1, result2)

    def stop_tmux(self):
        result1 = self.ssh_group.run_on_nodes(Commands.pool_cmd('stop-tmux.sh'))
        result2 = self.ssh_group.run_rpc_on_master(Commands.pool_cmd('stop-tmux.sh'))
        return self._get_result(result1, result2)

    def reset_chain(self):
        result1 = self.ssh_group.run_on_nodes(Commands.pool_cmd('reset-chain.sh'))
        result2 = self.ssh_group.run_rpc_on_master(Commands.pool_cmd('reset-chain-master.sh'))
        return self._get_result(result1, result2)

    def git_pull(self):
        result1 = self.ssh_group.run_on_nodes(Commands.pool_cmd('pull.sh'))
        result2 = self.ssh_group.run_on_master(Commands.pool_cmd('pull.sh'))
        return self._get_result(result1, result2)

    def rebuild_daemon(self):
        result1 = self.ssh_group.run_on_nodes(Commands.pool_cmd('rebuild-daemon.sh'))
        result2 = self.ssh_group.run_on_master(Commands.pool_cmd('rebuild-daemon.sh'))
        return self._get_result(result1, result2)

    def _get_result(self, nodes_result, master_result):
        result = Obj()
        result.nodes = nodes_result
        result.master = master_result
        result.node_errors = self.ssh_group.has_errors(nodes_result)
        result.master_errors = self.ssh_group.has_errors(master_result)
        result.errors = result.node_errors or result.master_errors
        return result


class Commands(object):

    HOME_DIR = "/root/"
    POOL_PREFIX = HOME_DIR + "pool/node-stratum-pool/"
    COIND_PREFIX = HOME_DIR + "coinds/bitcoin-sm/src/bitcoin-cli "

    CMD_INSTALL = "install.sh {} {}"

    def pool_cmd(command):
        return Commands.POOL_PREFIX + command

    def bitcoincli_cmd(command):
        return Commands.COIND_PREFIX + command.strip


if __name__ == '__main__':
    main()