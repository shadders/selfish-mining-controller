import ssh_helper

def main():
    pass

class Ssh_node(object):

    def __init__(self, id, user, host, password):
        self.conn_tmux = ssh_helper.Ssh_Helper(id + '_tmux', user, host, password)
        self.conn_main = ssh_helper.Ssh_Helper(id, user, host, password)


if __name__ == '__main__':
    main()